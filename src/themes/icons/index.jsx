import { star } from "./star.jsx";
import { youtube } from "./youtube.jsx";
import { cart } from "./cart.jsx";
import { minus } from "./minus.jsx";
import { plus } from "./plus.jsx";
import { home } from "./home.js";
import { arrow } from "./arrow.js";
import { compass } from "./compass.js";
import { heart } from "./heart.js";


export {
    star,
    youtube,
    cart,
    minus,
    plus,
    home,
    arrow,
    compass,
    heart
}